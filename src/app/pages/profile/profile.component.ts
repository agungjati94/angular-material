import { Component, OnInit } from '@angular/core';
import { ProfileModel } from 'src/app/models/profile.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  modelInput :ProfileModel = new ProfileModel()
  modelView :ProfileModel = new ProfileModel()
  
  constructor() { }

  ngOnInit() {
  }

  onViewBio()
  {
    this.modelView = {...this.modelInput}
  }

}
